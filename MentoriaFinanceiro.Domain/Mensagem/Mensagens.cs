﻿using System;
namespace MentoriaFinanceiro.Domain.Mensagem
{
    public class Mensagens
    {
        public const string OBJETO_NAO_INFORMADO = "Não conta o objeto";
        public const string CPF_INVALIDO = "CPF invalido";
        public const string MAIOR_18 = "Ë necessário ter mais de 18 anos.";
        public const string CAMPO_OBRIGATORIO = " é obrigatório.";
        public const string CPF_USO = " Este Cpf já esta em uso..";

        public const string CONTA_SALDO_INICIAL_NULL = " Informe um valor para o saldo inicial";
        public const string CONTA_SALDO_INICIAL_MAIOR_0 = " Informe um valor da abertura da conta";
        public const string CONTA_SEM_CORRENTISTA = " Para a abertura de conta informe o correntista";
        public const string CONTA_SEM_AGENCIA = " Informe a agencia.";
        public const string CONTA_SEM_CONTA = " Informe a conta corrente.";
        public const string CONTA_NAO_ENCONTRADA = "Conta não encontrada";
        public const string CONTA_JA_EXISTENTE = "Conta ja existente.";
        public const string CORRENTISTA_TEM_CONTA = "So poder uma conta por cpf.";

        public const string SALDO_INSUFICIENTE = "Saldo insuficiente para a transação.";


        public const string DEPOSITO_SUCESSO = "Deposito realizado com sucesso";
        public const string PAGAMENTO_SUCESSO = "Pagamento realizado com sucesso";
        public const string TRANSFERENCIA_SUCESSO = "Transferencia realizado com sucesso";

        public const string DEPOSITO_TEM_SER_MAIOR_0 = "Deposito tem que ser maior que zero";

        public const string MOVIMENTACAO_SEM_OPERACAO = "São foi identificada o tipo da operação";

    }
}
