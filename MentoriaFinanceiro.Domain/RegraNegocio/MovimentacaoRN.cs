﻿using System;
using MentoriaFinanceiro.Domain.Entities;
using MentoriaFinanceiro.Domain.Mensagem;

namespace MentoriaFinanceiro.Domain.RegraNegocio
{
    public class MovimentacaoRN
    {
        public MovimentacaoRN()
        {
        }

        public static void validadarMovimentacao(Movimentacao movimentacao)
        {
            if (movimentacao.ContaId.Equals(null))
            {
                throw new FormatException(Mensagens.CONTA_NAO_ENCONTRADA);
            }
            if (movimentacao.Conta.Id.Equals(null))
            {
                throw new FormatException(Mensagens.CONTA_NAO_ENCONTRADA);
            }
            if (movimentacao.ValorMovimentacao.Equals(0) || movimentacao.ValorMovimentacao < 0)
            {
                throw new FormatException(Mensagens.DEPOSITO_TEM_SER_MAIOR_0);
            }
            if (movimentacao.OperacaoId.Equals(null))
            {
                throw new FormatException(Mensagens.MOVIMENTACAO_SEM_OPERACAO);
            }
        }
    }
}
