﻿using System;
using MentoriaFinanceiro.Domain.Entities;
using MentoriaFinanceiro.Domain.Mensagem;

namespace MentoriaFinanceiro.Domain.RegraNegocio
{
    public class ContaRN
    {
        public ContaRN()
        {
        }

        public static Conta contaCadastro(int pessoaId, string agencia, string contaCorrente, decimal valorAbertura)
        {

            var conta = new Conta
            {
                PessoaId = pessoaId,
                Agencia = agencia,
                ContaCorrente = contaCorrente,
                DataAbertura = DateTime.Now,
                ValorAbertura = valorAbertura,
                ValorSaldoAtual = valorAbertura,
                DataUltimaMovimentacao = DateTime.Now,
                Ativo = true
            };

            return conta;

        }
        public static void validarAberturaConta(Conta conta)
        {
            if (conta == null)
            {
                throw new FormatException(Mensagens.OBJETO_NAO_INFORMADO + "conta.");
            }

            if (conta.ValorAbertura.Equals(null))
            {
                throw new FormatException(Mensagens.CONTA_SALDO_INICIAL_NULL);
            }
            if (conta.ValorAbertura.Equals(0))
            {
                throw new FormatException(Mensagens.CONTA_SALDO_INICIAL_MAIOR_0);
            }
            if (conta.ValorAbertura != conta.ValorSaldoAtual)
            {
                throw new FormatException(Mensagens.CONTA_SALDO_INICIAL_MAIOR_0);
            }

            if (conta.PessoaId.Equals(null))
            {
                throw new FormatException(Mensagens.CONTA_SEM_CORRENTISTA);
            }
            if (conta.Agencia.Equals(null))
            {
                throw new FormatException(Mensagens.CONTA_SEM_AGENCIA);
            }

            if (conta.Agencia.Equals(null))
            {
                throw new FormatException(Mensagens.CONTA_SEM_CONTA);
            }

        }

        public static void verificaSaldoConta(Conta conta, decimal valor)
        {
            if (conta == null)
            {
                throw new FormatException(Mensagens.OBJETO_NAO_INFORMADO + "conta.");
            }
            if (conta.ValorSaldoAtual < valor)
            {
                throw new FormatException(Mensagens.SALDO_INSUFICIENTE);
            }
        }
        public static void verificaDeposito(Conta conta)
        {
            if (conta == null)
            {
                throw new FormatException(Mensagens.OBJETO_NAO_INFORMADO + "conta.");
            }
        }
    }
}
