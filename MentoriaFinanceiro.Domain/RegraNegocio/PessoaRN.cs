﻿using System;
using MentoriaFinanceiro.Domain.Entities;
using MentoriaFinanceiro.Domain.Mensagem;

namespace MentoriaFinanceiro.Domain.RegraNegocio
{
    public class PessoaRN
    {
        public PessoaRN()
        {
        }

        public static void validarAddPessoa(Pessoa pessoa)
        {
            if (pessoa == null)
            {
                throw new FormatException(Mensagens.OBJETO_NAO_INFORMADO + " pessoa.");
            }
            if (!pessoa.DataNascimento.HasValue)
            {
                throw new FormatException("Data de nascimento  " + Mensagens.CAMPO_OBRIGATORIO);
            }
            if (pessoa.DataNascimento.Value.AddYears(18) > DateTime.Now)
            {
                throw new FormatException(Mensagens.MAIOR_18);
            }

        }
    }
}
