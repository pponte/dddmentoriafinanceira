﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MentoriaFinanceiro.Domain.Entities
{

    public class Extrato
    {

        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public decimal SaldoAnterior { get; set; }
        public decimal SaldoFinalPeriodo { get; set; }
        public decimal SaldoAtual { get; set; }
        public List<ExtratoMovimentacao> Movimentacao { get; set; }
    }

}
