﻿using System;
using System.Collections;
using System.Linq;
using MentoriaFinanceiro.Domain.Entities;

namespace MentoriaFinanceiro.Domain.Core.Interfaces.Repositories
{
    public interface IRepositoryPessoa : IRepositoryBase<Pessoa>
    {
        int CountByCpf(string cpf, int id = 0);
    }
}
