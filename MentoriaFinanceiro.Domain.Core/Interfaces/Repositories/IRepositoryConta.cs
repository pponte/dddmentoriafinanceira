﻿using System;
using System.Collections;
using System.Linq;
using MentoriaFinanceiro.Domain.Entities;


namespace MentoriaFinanceiro.Domain.Core.Interfaces.Repositories
{
    public interface IRepositoryConta : IRepositoryBase<Conta>
    {
        int VerificaPessoaTemConta(int pessoaID);
        Conta GetConta(string agencia, string conta);
    }
}
