﻿using MentoriaFinanceiro.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MentoriaFinanceiro.Domain.Core.Interfaces.Services
{
    public interface IServicePessoa : IServiceBase<Pessoa>
    {
        int CountByCpf(string cpf, int id = 0);
    }
}
