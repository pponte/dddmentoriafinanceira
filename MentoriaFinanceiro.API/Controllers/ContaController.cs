﻿using MentoriaFinanceiro.Application.Dtos;
using MentoriaFinanceiro.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace MentoriaFinanceiro.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ContaController : ControllerBase
    {
        private readonly IApplicationServiceConta _applicationServiceConta;

        public ContaController(IApplicationServiceConta ApplicationServiceConta)
        {
            _applicationServiceConta = ApplicationServiceConta;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(_applicationServiceConta.GetAll());
        }

        [HttpGet("{id}")]

        public ActionResult<string> Get(int id)
        {
            return Ok(_applicationServiceConta.GetAll());
        }

        [HttpPost("{pessoaId}, {agencia}, {ContaCorrente}, {valorAbertura}")]
        [ActionName("CriarConta")]
        public ActionResult Post(int pessoaId, string agencia, string ContaCorrente, decimal valorAbertura)
        {
            try
            {
                _applicationServiceConta.Add(pessoaId, agencia, ContaCorrente, valorAbertura);
                return Ok("Cadastro de conta realizado com sucesso!");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPut]
        public ActionResult Put([FromBody] ContaDto contaDto)
        {
            try
            {
                if (contaDto == null)
                    return NotFound();
                _applicationServiceConta.Update(contaDto);
                return Ok("Conta atualizada com sucesso!");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpDelete()]
        public ActionResult Delete([FromBody] ContaDto contaDto)
        {
            try
            {
                if (contaDto == null)
                    return NotFound();
                _applicationServiceConta.Remove(contaDto);
                return Ok("Conta removida com sucesso!");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("GetConta, {agencia}, {conta}")]
        [ActionName("GetConta")]
        public ActionResult GetConta(string agencia, string conta)
        {
            try
            {
                return Ok(_applicationServiceConta.GetConta(agencia, conta));
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("Extrato, {agencia}, {conta}, {dataInicio}, {dataFim}")]
        [ActionName("Extrato")]
        public ActionResult Extrato(string agencia, string conta, DateTime dataInicio, DateTime dataFim)
        {
            try
            {
                var resultado = _applicationServiceConta.Extrato(agencia, conta, dataInicio, dataFim);
                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

    }
}
