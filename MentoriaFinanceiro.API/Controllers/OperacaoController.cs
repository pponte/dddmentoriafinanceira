﻿using MentoriaFinanceiro.Application.Dtos;
using MentoriaFinanceiro.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace MentoriaFinanceiro.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OperacaoController : ControllerBase
    {
        private readonly IApplicationServiceOperacao _applicationServiceOperacao;
        public OperacaoController(IApplicationServiceOperacao ApplicationServiceOperacao)
        {
            _applicationServiceOperacao = ApplicationServiceOperacao;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(_applicationServiceOperacao.GetAll());
        }

        [HttpGet("{id}")]

        public ActionResult<string> Get(int id)
        {
            return Ok(_applicationServiceOperacao.GetById(id));
        }

    }
}
