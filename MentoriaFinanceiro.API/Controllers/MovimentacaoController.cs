﻿using MentoriaFinanceiro.Application.Dtos;
using MentoriaFinanceiro.Application.Interfaces;
using MentoriaFinanceiro.Domain.Mensagem;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;

namespace MentoriaFinanceiro.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MovimentacaoController : ControllerBase
    {
        private readonly IApplicationServiceMovimentacao _applicationServiceMovimentacao;

        public MovimentacaoController(IApplicationServiceMovimentacao ApplicationServiceConta)
        {
            _applicationServiceMovimentacao = ApplicationServiceConta;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(_applicationServiceMovimentacao.GetAll());
        }

        [HttpGet("{id}")]

        public ActionResult<string> Get(int id)
        {
            return Ok(_applicationServiceMovimentacao.GetAll());
        }


        [HttpPost("Deposito, {contaID}, {valor}")]
        [ActionName("Deposito")]
        public ActionResult Deposito(int contaID, decimal valor, string descricao)
        {
            try
            {
                _applicationServiceMovimentacao.Deposito(contaID, valor, descricao);
                return Ok(Mensagens.DEPOSITO_SUCESSO);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
                //return NotFound(ex.Message);
            }
        }

        [HttpPost("Pagamento, {contaID}, {valor}")]
        [ActionName("Pagamento")]
        public ActionResult Pagamento(int contaID, decimal valor, string descricao)
        {
            try
            {
                _applicationServiceMovimentacao.Pagamento(contaID, valor, descricao);
                return Ok(Mensagens.PAGAMENTO_SUCESSO);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
                //return NotFound(ex.Message);
            }
        }


        [HttpPost("transferencia, {contaID}, {valor}, {agenciaDestino}, {contaDestino}")]
        [ActionName("Transferencia")]
        public ActionResult Transferencia(int contaID, decimal valor, string agenciaDestino, string contaDestino, string descricao)
        {
            try
            {
                _applicationServiceMovimentacao.Transferencia(contaID, valor, agenciaDestino, contaDestino, descricao);
                return Ok(Mensagens.TRANSFERENCIA_SUCESSO);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
                //return NotFound(ex.Message);
            }
        }

    }
}
