﻿using MentoriaFinanceiro.Application.Dtos;
using MentoriaFinanceiro.Application.Interfaces;
using MentoriaFinanceiro.Application.Interfaces.Mappers;
using MentoriaFinanceiro.Domain.Core.Interfaces.Services;
using MentoriaFinanceiro.Domain.Entities;
using MentoriaFinanceiro.Domain.Mensagem;
using MentoriaFinanceiro.Domain.RegraNegocio;
using System;
using System.Collections.Generic;

namespace MentoriaFinanceiro.Application
{
    public class ApplicationServicePessoa : IApplicationServicePessoa
    {
        private readonly IServicePessoa servicePessoa;
        private readonly IMapperPessoa mapperPessoa;

        public ApplicationServicePessoa(IServicePessoa servicePessoa, IMapperPessoa mapperPessoa)
        {
            this.servicePessoa = servicePessoa;
            this.mapperPessoa = mapperPessoa;
        }
        public void Add(PessoaDto pessoaDto)
        {

            var pessoa = mapperPessoa.MapperDtoToEntity(pessoaDto);
            if (CountByCpf(pessoa.Cpf) > 0)
            {
                throw new FormatException(Mensagens.CPF_USO);
            }
            PessoaRN.validarAddPessoa(pessoa);
            servicePessoa.Add(pessoa);
        }

        public IEnumerable<PessoaDto> GetAll()
        {
            var pessoas = servicePessoa.GetAll();
            return mapperPessoa.MapperListPessoaDto(pessoas);
        }

        public PessoaDto GetById(int id)
        {
            var pessoas = servicePessoa.GetById(id);
            return mapperPessoa.MapperEntityToDto(pessoas);
        }

        public void Remove(PessoaDto pessoaDto)
        {
            var pessoa = mapperPessoa.MapperDtoToEntity(pessoaDto);
            servicePessoa.Remove(pessoa);
        }

        public void Update(PessoaDto pessoaDto)
        {
            var pessoa = mapperPessoa.MapperDtoToEntity(pessoaDto);
            if (CountByCpf(pessoa.Cpf, pessoa.Id) > 0)
            {
                throw new FormatException(Mensagens.CPF_USO);
            }
            servicePessoa.Update(pessoa);
        }

        public int CountByCpf(string cpf, int id = 0)
        {
            return servicePessoa.CountByCpf(cpf, id);
            //return mapperPessoa.MapperListPessoaDto(pessoas);
        }
    }
}
