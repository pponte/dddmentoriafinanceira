﻿using System;

namespace MentoriaFinanceiro.Application.Dtos
{
    public class ExtradoMovimentacaoDto
    {
        public int ContaId { get; set; }
        public DateTime DataMovimentacao { get; set; }
        public string ValorMovimentacao { get; set; }
        public string DescricaoMovimentacao { get; set; }
        public string TipoMovimentacao { get; set; }
        public string NomeOperacao { get; set; }

    }
}
