﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MentoriaFinanceiro.Application.Dtos
{
    public class ExtratoDto
    {
        public string Nome { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public decimal SaldoAnterior { get; set; }
        public decimal SaldoFinalPeriodo { get; set; }
        public List<ExtradoMovimentacaoDto> Movimentacao { get; set; }

    }
}
