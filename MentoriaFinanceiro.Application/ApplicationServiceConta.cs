﻿using MentoriaFinanceiro.Application.Dtos;
using MentoriaFinanceiro.Application.Interfaces;
using MentoriaFinanceiro.Application.Interfaces.Mappers;
using MentoriaFinanceiro.Domain.Core.Interfaces.Services;
using MentoriaFinanceiro.Domain.Entities;
using MentoriaFinanceiro.Domain.Mensagem;
using MentoriaFinanceiro.Domain.RegraNegocio;
using MentoriaFinanceiro.Domain.Services;
using System;
using System.Collections.Generic;

namespace MentoriaFinanceiro.Application
{
    public class ApplicationServiceConta : IApplicationServiceConta
    {
        private readonly IServiceConta serviceConta;
        private readonly IMapperConta mapperConta;
        private readonly IServiceMovimentacao serviceMovimentacao;
        private readonly IServicePessoa servicePessoa;

        public ApplicationServiceConta(IServiceConta serviceConta, IMapperConta mapperConta,
            IServiceMovimentacao serviceMovimentacao, IServicePessoa servicePessoa)
        {
            this.serviceConta = serviceConta;
            this.mapperConta = mapperConta;
            this.serviceConta = serviceConta;
            this.serviceMovimentacao = serviceMovimentacao;
            this.servicePessoa = servicePessoa;
        }

        public void Add(int pessoaId, string agencia, string contaCorrente, decimal valorAbertura)
        {

            var existeConta = GetConta(agencia, contaCorrente);

            if (!existeConta.Id.Equals(null))
            {
                throw new FormatException(Mensagens.CONTA_JA_EXISTENTE);
            }
            var conta = ContaRN.contaCadastro(pessoaId, agencia, contaCorrente, valorAbertura);


            ContaRN.validarAberturaConta(conta);

            if (VerificaPessoaTemConta(conta.PessoaId) > 0)
            {
                throw new FormatException(Mensagens.CORRENTISTA_TEM_CONTA);
            }

            serviceConta.Add(conta);

            var contaAberta = GetConta(agencia, contaCorrente);
            var movAbertura = new Movimentacao
            {
                ContaId = contaAberta.Id,
                Id = 0,
                OperacaoId = ApplicationServiceMovimentacao.DEPOSITO,
                DescricaoMovimentacao = "Abertura conta",
                DataMovimentacao = DateTime.Now,
                ValorMovimentacao = valorAbertura
            };

            serviceMovimentacao.Add(movAbertura);
        }
        public IEnumerable<ContaDto> GetAll()
        {
            var contas = serviceConta.GetAll();
            return mapperConta.MapperListContaDto(contas);
        }

        public ContaDto GetById(int id)
        {
            var contas = serviceConta.GetById(id);
            return mapperConta.MapperEntityToDto(contas);
        }
        public ContaDto GetConta(string agencia, string conta)
        {
            var contas = serviceConta.GetConta(agencia, conta);
            return mapperConta.MapperEntityToDto(contas);
        }


        public void Remove(ContaDto contaDto)
        {
            var pessoa = mapperConta.MapperDtoToEntity(contaDto);
            serviceConta.Remove(pessoa);
        }

        public void Update(ContaDto contaDto)
        {
            var conta = mapperConta.MapperDtoToEntity(contaDto);
            serviceConta.Update(conta);
        }


        public int VerificaPessoaTemConta(int pessoaID)
        {
            return serviceConta.VerificaPessoaTemConta(pessoaID);

        }

        public void depositarSaldoConta(Conta conta, decimal valor)
        {
            conta.ValorSaldoAtual += valor;
            serviceConta.Update(conta);

        }
        public void debitarSaldoConta(Conta conta, decimal valor)
        {
            conta.ValorSaldoAtual -= valor;
            serviceConta.Update(conta);

        }

        public Extrato Extrato(string agencia, string contaCorrente, DateTime dataInicio, DateTime dataFim)
        {
            var conta = GetConta(agencia, contaCorrente);

            var pessoa = servicePessoa.GetById(conta.PessoaId);

            var extrato = serviceMovimentacao.Extrato(mapperConta.MapperDtoToEntity(conta), dataInicio, dataFim);
            extrato.Agencia = conta.Agencia;
            extrato.Conta = conta.ContaCorrente;
            extrato.Nome = pessoa.Nome;
            extrato.Cpf = pessoa.Cpf;
            extrato.SaldoAtual = conta.ValorSaldoAtual;

            return extrato;
        }

    }
}
