﻿using MentoriaFinanceiro.Application.Dtos;
using MentoriaFinanceiro.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MentoriaFinanceiro.Application.Interfaces
{
    public interface IApplicationServiceConta
    {
        void Add(int pessoaId, string agencia, string ContaCorrente, decimal valorAbertura);

        void Remove(ContaDto contaDto);

        void Update(ContaDto contaDto);

        IEnumerable<ContaDto> GetAll();

        ContaDto GetById(int id);

        int VerificaPessoaTemConta(int pessoaID);

        void depositarSaldoConta(Conta conta, decimal valor);

        void debitarSaldoConta(Conta conta, decimal valor);

        ContaDto GetConta(String conta, String agencia);

        Extrato Extrato(string agencia, string conta, DateTime dataInicio, DateTime dataFim);


    }
}
