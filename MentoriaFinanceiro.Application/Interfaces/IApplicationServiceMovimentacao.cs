﻿using MentoriaFinanceiro.Application.Dtos;
using MentoriaFinanceiro.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MentoriaFinanceiro.Application.Interfaces
{
    public interface IApplicationServiceMovimentacao
    {
        void Add(MovimentacaoDto movimentacaoDto);
        void Remove(MovimentacaoDto movimentacaoDto);
        void Update(MovimentacaoDto movimentacaoDto);
        IEnumerable<MovimentacaoDto> GetAll();
        MovimentacaoDto GetById(int id);
        void Deposito(int contaID, decimal valor, string descricao);
        void Pagamento(int contaID, decimal valor, string descricao);
        void Transferencia(int contaID, decimal valor, string agenciaDestino, string contaDestino, string descricao);

        //decimal SaldoAnterior(int ContaID, DateTime dataInicio, DateTime dataFim);

    }
}
