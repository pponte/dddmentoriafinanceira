﻿using System;
using System.Collections.Generic;
using System.Linq;
using MentoriaFinanceiro.Domain.Core.Interfaces.Repositories;
using MentoriaFinanceiro.Domain.Entities;

namespace MentoriaFinanceiro.Infrastructure.Data.Repositories
{
    public class RepositoryPessoa : RepositoryBase<Pessoa>, IRepositoryPessoa
    {
        private readonly SqlContext sqlContext;

        public RepositoryPessoa(SqlContext sqlContext) : base(sqlContext)
        {
            this.sqlContext = sqlContext;
        }
        public int CountByCpf(string cpf, int id = 0)
        {
            if (id == 0)
            {
                return sqlContext.Set<Pessoa>()
               .Where<Pessoa>(Pessoa => Pessoa.Cpf == cpf)
               .Count();
            }
            else
            {
                return sqlContext.Set<Pessoa>()
               .Where<Pessoa>(Pessoa => Pessoa.Cpf == cpf)
               .Where<Pessoa>(Pessoa => Pessoa.Id != id)
               .Count();
            }

        }
    }
}
